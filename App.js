import React, { Component } from 'react';
import { StyleSheet, Text, View, Dimensions} from 'react-native';
import DeviceInfo from "react-native-device-info";
const deviceId = DeviceInfo.getDeviceId();
import MapView from 'react-native-maps';
export default class App extends Component {

  render() {
    const { region } = this.props;
    console.log(region);
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <MapView
            style={styles.map}
            region={{
              latitude: 37.78825,
              longitude: -122.4324,
              latitudeDelta: 0.015,
              longitudeDelta: 0.0121,
            }}
          >
          </MapView>
        </View>
        <View style={{ marginTop: Dimensions.get("window").height * 0.55, flex:1,justifyContent:'flex-start'}}>
          <Text>{deviceId}</Text>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  content: {
    marginTop: 20,
    ...StyleSheet.absoluteFillObject,
    height: Dimensions.get("window").height * 0.5,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
